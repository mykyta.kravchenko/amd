define('multiply', [], async function(){
  function multiply(a, b){
      return a * b
  }
  await new Promise(res => setTimeout(res, 2000));
  return multiply;
});
