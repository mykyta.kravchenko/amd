window.define = function(...args) {
  const [moduleFn, dependenciesNames, moduleName] = args.reverse();

  const dependenciesModules = dependenciesNames.map(el => Promise.resolve(define.cache[el]));
   

  const module = Promise.all(dependenciesModules).then(array =>  moduleFn(...array));

  if (moduleName) {
    define.cache[moduleName] = module;
  }
}

// define.queue = [];
define.cache = {};
